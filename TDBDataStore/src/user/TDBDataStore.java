package user;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.FileManager;

public class TDBDataStore {
	String DB_DIRECTORY = "D:\\Eclipse_workspaces\\SPARQL\\tdb";
	
	/**
	 * Extracts the information from a RDF file and stores it into the TDB data store
	 * @param filePath	The URL or the filepath of the Triple file
	 * @param rdfSyntax The rdfSyntax used
	 */
	public void StoreTDB(String filePath, String rdfSyntax) {
		// load data about the person from DBPedia into an in-memory RDF model
		Model person = FileManager.get().loadModel(filePath,rdfSyntax);
		
		// creating a new / opening an existing DB model
		Model model = TDBFactory.createModel(DB_DIRECTORY);
		// adding loaded data to DB model
		model.add(person);
		// print out what is in the model at the moment
		model.write(System.out, rdfSyntax);
		// close the DB model
		model.close();
	}
	
	/**
	 * Method to run a sparql query against TDB store to fetch the data
	 * @param queryString	sparql query
	 * @return	The results of the execution
	 */
	public String LoadFromDB(String queryString){
    	//opening an existing DB model
		Model tdb = TDBFactory.createModel(DB_DIRECTORY);

        Query query = QueryFactory.create(queryString);
        QueryExecution qexec = QueryExecutionFactory.create(query, tdb);
        ResultSet results = qexec.execSelect();
        String resultSet = ResultSetFormatter.asText(results);
        
        System.out.println("Result Set: " +resultSet);        
        System.out.println("Query successful");
        
        if(resultSet != null && resultSet.lastIndexOf("\"") > -1){
        	resultSet = resultSet.substring(resultSet.indexOf("\"", 0)+1, resultSet.lastIndexOf("\""));
        }
        return resultSet;
	}
	
//	public static void main(String[] args) {
//		String q =
//				"	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
//				"	PREFIX dbprop: <http://dbpedia.org/property/>" +
//						"   SELECT ?DOB"+
//						"   WHERE {  " +
//						"       			<http://dbpedia.org/resource/Jack_Nicholson>	dbprop:birthDate	?DOB." +		
//						"        }";
//
//		
//		TDBDataStore demo = new TDBDataStore();
//		demo.StoreTDB("file:///C:/Temp/Jack_Nicholson.nt", "N-TRIPLES");
//		demo.LoadFromDB(q);
//	}
	
}