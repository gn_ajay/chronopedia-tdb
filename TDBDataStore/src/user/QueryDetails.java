package user;

public class QueryDetails {
	private String person;
	private String fromDate;
	private String toDate;
	private String place;
	private String event;
	private String sparqlQuery;

	public String getPerson() {
		return person;
	}
	public void setPerson(String personName) {
		this.person = personName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getSparqlQuery() {
		return sparqlQuery;
	}
	public void setSparqlQuery(String sparqlQuery) {
		this.sparqlQuery = sparqlQuery;
	}

	public String getResults(String sparqlQuery){
		String queryProp = "birthDate";

		if(sparqlQuery != null && sparqlQuery.length() == 0){
			sparqlQuery =
					"	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
							"	PREFIX dbprop: <http://dbpedia.org/property/>" +
							"   SELECT ?DOB"+
							"   WHERE {  " +
							"       			<http://dbpedia.org/resource/"+person	+">dbprop:"+ queryProp + "?DOB." +		
							"        }";
		}
		
		

		TDBDataStore tdbStore = new TDBDataStore();
		String result = tdbStore.LoadFromDB(sparqlQuery);
		return result;
	}

}
