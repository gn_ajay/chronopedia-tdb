<%@ page contentType="text/html; charset=iso-8859-1" language="java" %>
<html>
<head>
<title>Chronopedia</title>
<meta name="generator" content="">
<style type="text/css">
/*----------Text Styles----------*/
.ws6 {font-size: 8px;}
.ws7 {font-size: 9.3px;}
.ws8 {font-size: 11px;}
.ws9 {font-size: 12px;}
.ws10 {font-size: 13px;}
.ws11 {font-size: 15px;}
.ws12 {font-size: 16px;}
.ws14 {font-size: 19px;}
.ws16 {font-size: 21px;}
.ws18 {font-size: 24px;}
.ws20 {font-size: 27px;}
.ws22 {font-size: 29px;}
.ws24 {font-size: 32px;}
.ws26 {font-size: 35px;}
.ws28 {font-size: 37px;}
.ws36 {font-size: 48px;}
.ws48 {font-size: 64px;}
.ws72 {font-size: 96px;}
.wpmd {font-size: 13px;font-family: Arial,Helvetica,Sans-Serif;font-style: normal;font-weight: normal;}
/*----------Para Styles----------*/
DIV,UL,OL /* Left */
{
 margin-top: 0px;
 margin-bottom: 0px;
}
</style>

</head>
<body background="bground.gif" bgcolor="C7E4F2" >
<form name="frm" method="get" action="input.jsp">
<div id="text1" style="position:absolute; overflow:hidden; left:563px; top:29px; width:330px; height:75px; z-index:0">
<div class="wpmd">
<div><font color="7A7A79" face="Algerian" class="ws36">CHRONOPEDIA</font></div>
</div></div>

<div style="position:absolute; left:100px; top:20px; width:1350px;height:770px; border:2px solid #000"></div>

<div id="text2" style="position:absolute; overflow:hidden; left:177px; top:160px; width:68px; height:34px; z-index:1">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">Person:</font></div>
</div></div>

<div id="text3" style="position:absolute; overflow:hidden; left:968px; top:160px; width:109px; height:32px; z-index:2">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">Date (From):</font></div>
</div></div>

<div id="text4" style="position:absolute; overflow:hidden; left:609px; top:160px; width:82px; height:27px; z-index:3">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">Date (To):</font></div>
</div></div>

<div id="text5" style="position:absolute; overflow:hidden; left:175px; top:220px; width:77px; height:31px; z-index:4">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">Place:</font></div>
</div></div>

<div id="text6" style="position:absolute; overflow:hidden; left:606px; top:220px; width:69px; height:27px; z-index:5">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">Event:</font></div>
</div></div>

<div id="text7" style="position:absolute; overflow:hidden; left:174px; top:280px; width:150px; height:34px; z-index:6">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">SPARQL Query:</font></div>
</div></div>

<div id="text8" style="position:absolute; overflow:hidden; left:175px; top:520px; width:84px; height:33px; z-index:7">
<div class="wpmd">
<div><font face="Times New Roman" class="ws14">Results:</font></div>
</div></div>

<input name="person" type="text" style="position:absolute;left:260px;top:160px;width:225px;z-index:8;background-color:#F2F2F2">
<input name="fromDate" type="text" style="position:absolute;width:200px;left:704px;top:160px;z-index:9;background-color:#F2F2F2">
<input name="toDate" type="text" style="position:absolute;width:200px;left:1085px;top:160px;z-index:10;background-color:#F2F2F2">
<input name="place" type="text" style="position:absolute;width:226px;left:257px;top:220px;z-index:11;background-color:#F2F2F2">
<input name="event" type="text" style="position:absolute;width:200px;left:705px;top:220px;z-index:12;background-color:#F2F2F2">
<textarea name="sparqlQuery" style="position:absolute;left:375px;top:280px;width:840px;height:200px;z-index:13;background-color:#F2F2F2"></textarea>
<input name="submitQuery" type="submit" style="position:absolute;left:1120px;top:485px;z-index:14;height:25" value="Submit Query">
<textarea name="textarea2" style="position:absolute;left:375px;top:520px;width:840px;height:250px;z-index:15;background-color:#F2F2F2"></textarea>

</form>
</body>
</html>

